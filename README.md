# ADS-B Kinematics
ADS-B kinematics, a Python implementation of decoding and analysis for ADS-B messages

This project was created by:

S. Ali, M. Dabrowski , F. Dahmani , T. Dijkstra , J. L’Ortije , Y. Oei , L. Oerlemans , D. Schwering , B. Sutar and N. Wechtler
members of CD-14 for the Test, Analysis and Simulation project at the Aerospace engineering faculty at TU Delft.

## Usage
Unfortunately this repository was not a deliverable and is therefore not cleanly maintained.

The processes described in the method section of the paper are worked into exporter, importer and statplotter.
