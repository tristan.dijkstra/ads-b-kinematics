"""
This file imports data from the exportedData folder and makes Statistical plots for cruise speed, rate of climb,
cruise altitude.
"""

import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import pandas as pd
import os
from importer import importData
import seaborn as sns
from tqdm import tqdm
from astropy.visualization import hist

boxPlotExportDir = "StatPlots/Boxplots/"
histoExportDir = "StatPlots/Histograms/"
spiderExportDir = "StatPlots/SpiderPlots/"
saveFiles = "StatPlots/saves/"

# Make folder for boxplots
if not os.path.exists(boxPlotExportDir):
    os.makedirs(boxPlotExportDir)

# Make folder for histograms
if not os.path.exists(histoExportDir):
    os.makedirs(histoExportDir)

# Make folder for spider plots
if not os.path.exists(spiderExportDir):
    os.makedirs(spiderExportDir)

# Make folder for spider plots
if not os.path.exists(saveFiles):
    os.makedirs(saveFiles)


def kolmogorovTest(dataframe, axis):
    """
    Performs kolmogorov Test for normal, beta and gamma distribution

    :param dataframe: Pandas dataframe
    :param axis: axis to be analyzed
    :return: Depends on distribution
    """

    dataframe = dataframe.reset_index(drop=True)
    dataframe.index = dataframe.index + 1  # Reset indices to start at 1
    dataframe['eCDF'] = dataframe.index / len(dataframe[axis])  # Empirical cumulative distribution function based on data

    # Set up normal distribution from data set
    normalMean, normalStd = stats.norm.fit(dataframe[axis])  # Fit data to normal distribution
    normalCdf = stats.norm.cdf(dataframe[axis], normalMean, normalStd)  # Normal cumulative distribution function
    normalMaxDifference = max(abs(dataframe['eCDF'] - normalCdf))  # Max diff between eCDF and normal CDF

    # Set up beta distribution from data set
    betaAlpha, betaBeta, betaLoc, betaScale = stats.beta.fit(dataframe[axis])  # Fit beta distribution
    betaCdf = stats.beta.cdf(dataframe[axis], betaAlpha, betaBeta, betaLoc, betaScale)  # Beta cumulative distribution
    betaMaxDifference = max(abs(dataframe['eCDF'] - betaCdf))  # Max diff eCDF and beta CDF

    # Set up gamma distribution from data set
    gammaAlpha, gammaLoc, gammaScale = stats.gamma.fit(dataframe[axis])  # Fit gamma distribution
    gammaCdf = stats.gamma.cdf(dataframe[axis], gammaAlpha, gammaLoc, gammaScale)  # Gamma cumulative distribution
    gammaMaxDifference = max(abs(dataframe['eCDF'] - gammaCdf))  # Max diff eCDF and gamma CDF

    bestDistribution = min(normalMaxDifference, betaMaxDifference, gammaMaxDifference)  # Find best distribution
    mean = normalMean  # Needed for graph

    if bestDistribution == normalMaxDifference:
        normalFunc = stats.norm.pdf(dataframe[axis], normalMean, normalStd)  # Make the PDF for normal distribution
        oneSigmaCI = stats.norm.interval(0.68, normalMean, normalStd)  # Find 1 sigma confidence interval
        twoSigmaCI = stats.norm.interval(0.92, normalMean, normalStd)  # Find 2 sigma confidence interval
        threeSigmaCI = stats.norm.interval(0.997, normalMean, normalStd)  # Find 3 sigma confidence interval
        return mean, normalStd, normalMean, normalMaxDifference, normalFunc, normalCdf, oneSigmaCI, twoSigmaCI, \
               threeSigmaCI, "Normal Distribution"

    elif bestDistribution == betaMaxDifference:
        betaFunc = stats.beta.pdf(dataframe[axis], betaAlpha, betaBeta, betaLoc, betaScale)  # Make Beta PDF
        oneSigmaCI = stats.beta.interval(0.68, betaAlpha, betaBeta, betaLoc, betaScale)  # 1 sigma CI
        twoSigmaCI = stats.beta.interval(0.92, betaAlpha, betaBeta, betaLoc, betaScale)  # 2 sigma CI
        threeSigmaCI = stats.beta.interval(0.997, betaAlpha, betaBeta, betaLoc, betaScale)  # 3 sigma CI
        return mean, betaScale, betaLoc, betaMaxDifference, betaFunc, betaCdf, oneSigmaCI, twoSigmaCI, \
               threeSigmaCI, "Beta Distribution", betaAlpha, betaBeta

    elif bestDistribution == gammaMaxDifference:
        gammaFunc = stats.gamma.pdf(dataframe[axis], gammaAlpha, gammaLoc, gammaScale)  # Make Gamma PDF
        oneSigmaCI = stats.gamma.interval(0.68, gammaAlpha, gammaLoc, gammaScale)  # 1 sigma CI
        twoSigmaCI = stats.gamma.interval(0.92, gammaAlpha, gammaLoc, gammaScale)  # 2 sigma CI
        threeSigmaCI = stats.gamma.interval(0.997, gammaAlpha, gammaLoc, gammaScale)  # 3 sigma CI
        return mean, gammaScale, gammaLoc, gammaMaxDifference, gammaFunc, gammaCdf, oneSigmaCI, twoSigmaCI, \
               threeSigmaCI, "Gamma Distribution", gammaAlpha


def statPlotter(dataframe, axis, aircraft, spec, axisLabel, n=50, binAlg='scott', autoBins=True, showFig=False):
    """
    Plots Graphs

    :param dataframe: Pandas Dataframe containing aircraft data
    :param axis: Dataframe axis to be analyzed  (string)
    :param aircraft: aircraft name or ICAO code to save files by (string)
    :param spec: what kind of data is being analyzed e.g. Cruise speed (string)
    :param axisLabel: Label the x axis of the plot  (string)
    :param n: If autobins=False this determines the number of bins (int)
    :param binAlg: Algorithm to determine bin width, default=blocks
    :param autoBins: Toggles between automatically determining bin width (Bool)
    :param showFig: preview of figure (Bool)
    :return: Saves Figure

    Dataframe format:
        aircraft1   aircraft2
    0   val1        val1
    1   val2        val2
    2   val3        val3

    Can only do 1 column at a time so specify axis e.g. axis="aircraft1"
    """

    kolmo = kolmogorovTest(dataframe.sort_values(axis), axis)

    # Make Histogram plot
    sns.set(style='whitegrid')
    if autoBins:
        hist(dataframe[axis], bins=binAlg, density=True, color='dimgrey', alpha=0.75,
             edgecolor="black", linewidth=1.2)
        bins = "auto"
    else:
        hist(dataframe[axis], n, density=True, color='dimgrey', alpha=0.75,
             edgecolor="black", linewidth=1.2)
        bins = "manual_{}".format(str(n))

    plt.plot(dataframe.sort_values(axis)[axis], kolmo[4], color='red', alpha=0.9, linewidth=1.2)
    plt.xlabel(axisLabel, fontsize=14)
    plt.ylabel("Probability", fontsize=14)
    plt.xticks(fontsize=12, rotation=0)
    plt.yticks(fontsize=12, rotation=0)
    plt.legend([kolmo[9], "{} data".format(aircraft)], fontsize=14)
    plt.title("{} {}".format(aircraft, spec))
    plt.figtext(0.92, 0.80, "\u03BC = {}".format(round(kolmo[0], 2)), fontsize=14)
    plt.figtext(0.92, 0.26, "Confidence Intervals:", fontsize=14)
    plt.figtext(0.92, 0.22, "1\u03C3 = ({}, {})".format(round(kolmo[6][0], 2), round(kolmo[6][1], 2)), fontsize=14)  # 1 Sigma CI
    plt.figtext(0.92, 0.18, "2\u03C3 = ({}, {})".format(round(kolmo[7][0], 2), round(kolmo[7][1], 2)), fontsize=14)  # 2 Sigma CI
    plt.figtext(0.92, 0.14, "3\u03C3 = ({}, {})".format(round(kolmo[8][0], 2), round(kolmo[8][1], 2)), fontsize=14)  # 3 Sigma CI
    plt.figtext(0.92, 0.48, "Error:", fontsize=14)
    plt.figtext(0.92, 0.44, "max|eCDF - CDF| = {}".format(round(kolmo[3], 3)), fontsize=14)

    if kolmo[9].lower().startswith("n"):
        plt.figtext(0.92, 0.84, "{}:".format(kolmo[9]), fontsize=14)
        plt.figtext(0.92, 0.76, "\u03C3 = {}".format(round(kolmo[1], 2)), fontsize=14)

    elif kolmo[9].lower().startswith("b"):
        plt.figtext(0.92, 0.84, "{}:".format(kolmo[9]), fontsize=14)
        plt.figtext(0.92, 0.76, "\u03B1 = {}".format(round(kolmo[10], 2)), fontsize=14)
        plt.figtext(0.92, 0.72, "\u03B2 = {}".format(round(kolmo[11], 2)), fontsize=14)
        plt.figtext(0.92, 0.68, "loc = {}".format(round(kolmo[2], 2)), fontsize=14)
        plt.figtext(0.92, 0.64, "scale = {}".format(round(kolmo[1], 2)), fontsize=14)

    elif kolmo[9].lower().startswith("g"):
        plt.figtext(0.92, 0.84, "{}:".format(kolmo[9]), fontsize=14)
        plt.figtext(0.92, 0.76, "\u03B1 = {}".format(round(kolmo[10], 2)), fontsize=14)
        plt.figtext(0.92, 0.72, "loc = {}".format(round(kolmo[2], 2)), fontsize=14)
        plt.figtext(0.92, 0.68, "scale = {}".format(round(kolmo[1], 2)), fontsize=14)

    plt.savefig("{}{}_{}_{}".format(histoExportDir, aircraft, spec, bins), dpi=1000, bbox_inches='tight')
    print("Saved Figure under:{}{}_{}_{}".format(histoExportDir, aircraft, spec, bins))

    showFigure(showFig)
    plt.close()


def boxPlot(dataframe, aircraft, spec, axisLabel, outliers=True, showFig=False):
    """

    :param dataframe:   Pandas Dataframe
    :param aircraft:    aircraft name (string)
    :param spec:        Type of spec (e.g. cruise speed) (string)
    :param axisLabel:   Axis label for the plot (string)
    :param outliers:    Show outliers in boxplot (Bool)
    :param showFig:     Should the figure be shown (Bool)
    :return:            Saves Boxplot

    Dataframe layout:

            aircraft1   aircraft2
    0       val1        val1
    1       val2        val2
    2       val3        val3...
    """

    # Make Boxplot
    sns.set_palette("PuBuGn_d")
    sns.set(style='whitegrid')
    sns.boxplot(data=dataframe, orient='v', width=0.5, medianprops={'color': 'black'},
                boxprops=dict(alpha=.75), showfliers=outliers)
    plt.ylabel(axisLabel, fontsize=14)
    plt.xticks(fontsize=14, rotation=0)
    plt.yticks(fontsize=14, rotation=0)
    plt.title("{} and {}, {} comparison".format(aircraft[0], aircraft[1], spec), y=1.10)
    plt.savefig("{}{}_vs_{}_{}_outliers={}".format(boxPlotExportDir, aircraft[0], aircraft[1], spec, outliers),
                dpi=1000, bbox_inches='tight')
    print("Saved Figure under: {}{}_vs_{}_{}_outliers={}".format(boxPlotExportDir, aircraft[0], aircraft[1], spec,
                                                                 outliers))

    showFigure(showFig)
    plt.close()


def spiderPlot(dataframe, aircraft, label, showFig=False):

    """
    :param dataframe: pandas dataframe containing data
    :param aircraft: list of aircraft names in order of data
    :param showFig: should the figure be shown
    :return: Saves spider plot to folder

    Dataframe Layout:

                param1  param2  param3  param4
    aircraft1   val     val     val     val
    aircraft2   val     val     val     val

    Two rows is the max as adding more makes the plots hard to interpret
    """

    # number of variable
    categories = list(dataframe)[0:]
    N = len(categories)

    # What will be the angle of each axis in the plot? (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * np.pi for n in range(N)]
    angles += angles[:1]

    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)

    # If you want the first axis to be on top:
    ax.set_theta_offset(np.pi / 2)
    ax.set_theta_direction(-1)

    # Draw one axe per variable + add labels labels yet
    plt.xticks(angles[:-1], categories)
    maximum = max(dataframe.max())

    # Draw ylabels
    ax.set_rlabel_position(0)
    plt.yticks([round(maximum / 3, 2), round(maximum * 2 / 3, 2), round(maximum, 2)], [str(round(maximum / 3, 2)),
                str(round(maximum * 2 / 3, 2)), str(round(maximum, 2))], color="grey", size=12)
    plt.ylim(0, maximum)

    # Entry 1
    values = dataframe.loc[mdls[0]].values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=aircraft[0])
    ax.fill(angles, values, 'b', alpha=0.1)

    # Entry 2
    values = dataframe.loc[mdls[1]].values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=aircraft[1])
    ax.fill(angles, values, 'r', alpha=0.1)

    # Add legend
    plt.legend(loc='upper right', bbox_to_anchor=(0.1, 0.1))
    plt.title("Performance comparison {} and {}".format(aircraft[0], aircraft[1]), y=1.08)
    plt.savefig("{}{}_vs_{}_{}".format(spiderExportDir, aircraft[0], aircraft[1], label), dpi=1000, bbox_inches='tight')
    print("Saved Figure under:{}{}_vs_{}".format(spiderExportDir, aircraft[0], aircraft[1]))
    showFigure(showFig)
    plt.close()


def showFigure(showFig):
    if showFig:
        plt.show()


if __name__ == '__main__':

    labels = np.array([[[i for i in range(9, 23)], "Altitude", "alt", "Altitude [m]", "C", 1],
                       [[19], "Climb Rate", "climb", "Rate of Climb [m/s]", "A", 0],
                       [[19], "Descent Rate", "climb", "Rate of Descent [m/s]", "D", 2],
                       [[19], "Ground speed", "speed", "Ground Speed [m/s]", "C", 1]])

    result = None
    mdls = None

    aircraft = [["a320", "a320"],
                ["b738", "737-800"]]

    days = [i for i in range(1, 31)]

    spiderDf = pd.DataFrame([[0, 0, 0, 0], [0, 0, 0, 0]])               # Initialize spiderplot dataframe
    spiderDf.columns = ["Speed", "ROC", "ROD", "Altitude"]
    spiderDf.rename(index={0: "a320", 1: "737-800"}, inplace=True)

    spiderDfMin = pd.DataFrame([[0, 0, 0, 0], [0, 0, 0, 0]])            # Initialize spiderplot dataframe
    spiderDfMin.columns = ["Speed", "ROC", "ROD", "Altitude"]
    spiderDfMin.rename(index={0: "a320", 1: "737-800"}, inplace=True)

    spiderDfMax = pd.DataFrame([[0, 0, 0, 0], [0, 0, 0, 0]])            # Initialize spiderplot dataframe
    spiderDfMax.columns = ["Speed", "ROC", "ROD", "Altitude"]
    spiderDfMax.rename(index={0: "a320", 1: "737-800"}, inplace=True)

    for label in tqdm(labels):

        oldLabel = label[2]
        boxDf = []
        mdls = []

        for mdl in aircraft:
            label[2] = oldLabel
            concat = []  # Empty List for data frames containing the data
            mdls.append(mdl[1])

            for day in tqdm(days):
                try:
                    concat.append(pd.DataFrame(importData(mdl, label[0], [day], [[], []], [], [],
                                                          [label[4]])[0][int(label[5])][1]))
                except ValueError:
                    continue

            result = pd.concat(concat, ignore_index=True)  # Make one large data frame from all the data
            result = pd.DataFrame(result[label[2]])
            result.columns = [str(label[2])]

            if label[2] == "climb":
                result[label[2]] = result[label[2]] * 0.3048 / 60.  # ft/min --> m/s

                if label[4] == "D":
                    result[label[2]] = result[result[label[2]] < -0]    # filters
                    result[label[2]] = result[result[label[2]] > -35]   # filters
                    spiderDfMin['ROD'][mdl[1]] = result[label[2]].max(skipna=True)
                    spiderDf['ROD'][mdl[1]] = result[label[2]].mean(skipna=True)
                    spiderDfMax['ROD'][mdl[1]] = result[label[2]].min(skipna=True)
                    label[2] = "descent"
                    result.columns = [str(label[2])]

                else:
                    result[label[2]] = result[result[label[2]] > 0]
                    result[label[2]] = result[result[label[2]] < 35]
                    spiderDfMin['ROC'][mdl[1]] = result[label[2]].min(skipna=True)
                    spiderDf['ROC'][mdl[1]] = result[label[2]].mean(skipna=True)
                    spiderDfMax['ROC'][mdl[1]] = result[label[2]].max(skipna=True)

            if label[2] == "speed":
                result[label[2]] = result[label[2]] * 0.514444444       # kts --> m/s
                result[label[2]] = result[result[label[2]] < 325]       # filters
                spiderDfMin['Speed'][mdl[1]] = result[label[2]].min(skipna=True)
                spiderDf['Speed'][mdl[1]] = result[label[2]].mean(skipna=True)
                spiderDfMax['Speed'][mdl[1]] = result[label[2]].max(skipna=True)

            if label[2] == "alt":
                result[label[2]] = result[label[2]] * 0.3048            # ft --> m
                result[label[2]] = result[result[label[2]] > 8000]
                spiderDfMin['Altitude'][mdl[1]] = result[label[2]].min(skipna=True)
                spiderDf['Altitude'][mdl[1]] = result[label[2]].mean(skipna=True)
                spiderDfMax['Altitude'][mdl[1]] = result[label[2]].max(skipna=True)

            result = result.dropna(axis=0)
            boxDf.append(result)

            pd.DataFrame.to_csv(result, "{}{}_{}".format(saveFiles, mdl[1], label[1]))
            statPlotter(result, label[2], mdl[1], label[1], label[3])    # statplots
            statPlotter(result, label[2], mdl[1], label[1], label[3], autoBins=False)  # statplots
            statPlotter(result, label[2], mdl[1], label[1], label[3], n=20, autoBins=False)  # statplots

        boxDf[0].reset_index(drop=True, inplace=True)
        boxDf[1].reset_index(drop=True, inplace=True)
        boxDfpd = pd.concat(boxDf, ignore_index=True, axis=1)
        boxDfpd.columns = mdls
        pd.DataFrame.to_csv(boxDfpd, "{}{}_Boxplot".format(saveFiles, label[2]))
        boxPlot(boxDfpd, mdls, label[2], label[3], outliers=True)       # boxplots with outliers
        boxPlot(boxDfpd, mdls, label[2], label[3], outliers=False)      # boxplots without outliers

    # Generate spiders plots
    for column in spiderDf:

        if (spiderDf[column] > 0).all():
            spiderDf[column] = spiderDf[column] / spiderDf[column].max()
            spiderDfMax[column] = spiderDfMax[column] / spiderDfMax[column].max()
            spiderDfMin[column] = spiderDfMin[column] / spiderDfMin[column].max()

        else:
            spiderDf[column] = spiderDf[column] / spiderDf[column].min()
            spiderDfMax[column] = spiderDfMax[column] / spiderDfMax[column].min()
            spiderDfMin[column] = spiderDfMin[column] / spiderDfMin[column].min()

    spiderDf = spiderDf.fillna(0)
    spiderDfMin = spiderDfMin.fillna(0)
    spiderDfMax = spiderDfMax.fillna(0)

    pd.DataFrame.to_csv(spiderDf, "{}_spiderAvg".format(saveFiles))
    pd.DataFrame.to_csv(spiderDfMax, "{}_spiderMax".format(saveFiles))
    pd.DataFrame.to_csv(spiderDfMin, "{}_spiderMin".format(saveFiles))

    spiderPlot(spiderDf, mdls, 'avg')
    spiderPlot(spiderDfMin, mdls, 'min')
    spiderPlot(spiderDfMax, mdls, 'max')
