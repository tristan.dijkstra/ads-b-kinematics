import os
import numpy as np
import pandas as pd
import datetime
import time
from exporter import dfTimeToNormal
from stdatmos import ISA
from math import sqrt
from tqdm import tqdm
"""
This file is the real importer file, that collects files from every possible subfolder 
in "./exportedData".
"""

#Importing parameters
aircraft = ["dh8d","a320","b738"]
Days = list(range(16,31))
PrefICAO = []
PrefFlight = []
Times = [[],[]]
TypeCodes = [11,19]
PrefPhase = ["A","C","D"]

#List of typecodes and their corresponding category:
#callsignCodes = [1, 2, 3, 4]
#position2DCodes = [5, 6, 7, 8]
#position3DCodes = [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22]
#velocityCodes = [19]
#statusInfoCodes = [29, 31]

#sample parameters:
#aircraft = ["dh8d", "a320"]
#Days = [1]
#PrefICAO = []
#PrefFlight = []
#Times = [[9,57,11],[10, 0, 30]]
#TypeCodes = [2]

#Changes to be made: 
#1) Account for the fact that the .csv file names will change after merging this branch => change the condition statements where file names are used.
#2)Change acData to the following format once the exported data has been cut up accordign to flight phases:
#       acData= [  [(phase1, data1), (phase2, data2),...], [(phase1, data1), (phase2, data2),...],...  ]
#                  [                date1               ], [                date1               ],...
#3) Currently the program only works if the "Days" parameter is not left empty. If it is left empty the resulting dataframe set is empty, despite the 
#   setting of the default values in the "importData" function.

importDir = "FilteredData/"
if not os.path.exists(importDir):
    os.makedirs(importDir)

def MachNumber(df):
    Mach=[]
    speed=0
    altitude=0
    for i in range (len(df)):
        if df.loc[i,"tc"] == 19:
            speed = (df.loc[i,"speed"])* 0.514444
        if df.loc[i,"tc"] == 11:
            altitude= (df.loc[i,"alt"]) *0.3048
        #print(altitude,speed)
        ISAStuff=ISA(altitude)
        temperature= ISAStuff[2]
        MachNumber= speed / (np.sqrt(1.4 * 287 *temperature))
        Mach.append(MachNumber)
    df["Mach"]=Mach
    return(df)

def Range(df,first):
    RangeList=[]
    Init = df.loc[first,"unix"]
    for i,row in df.iterrows():
        if df.loc[i,"tc"]== 19:
            speed = df.loc[i,"speed"] * 101.269
            ROC = df.loc[i,"climb"]
            t1 = (int(Init.strftime("%H"))*60) + int(Init.strftime("%M")) + (int(Init.strftime("%S"))/60)
            t2 = (int(df.loc[i,"unix"].strftime("%H"))*60) + int(df.loc[i,"unix"].strftime("%M")) + (int(df.loc[i,"unix"].strftime("%S"))/60)
            dt = t2-t1
            Range= sqrt(abs((speed**2)-(ROC**2))) * dt
            #print(speed,ROC,Range)
            Init=df.loc[i,"unix"]
            RangeList.append(Range)
    return sum(RangeList)

def importData(
    ac,      
    typeCodes,      # Default: All TCs (see expample 3 below)
    days,           # 1st-30th of April
    times,
    prefICAO,
    prefFlight,
    prefPhase
    ):
    """[imports all files that correspond to the given set of filters and sorts them 
       according to date]
    
    Arguments:
        ac {list of strings} -- [user-specified aircract type]
        typeCodes {list of int} -- [allows user to import ADS-B data pertaining to "pos","vel", "id", or "status"]
        days {list of int} -- [allows user to import data for a specific set of days in April]
        times {2D list of int} -- [allows user to import data for a specific interval of a day]
        prefICAO {list of strings} -- [user-specified ICAO, whose data is to be imported]
        prefFlight {list of strings} -- [user-specified callsign, whose data is to be imported]
    
    Returns:
        acData {list of pandas.DataFrame} -- [a list of filtered dataframes. Each element of this list 
                                              is a dataframe correponding to a single day in function 
                                              parameter "days"]
    """

    #Setting default values:
    if not len(typeCodes):
        typeCodes = list(range(1,23))+[29,31]
    if not len(times[0]):
        times[0] = [0,0,0]
    if not len(times[1]):
        times[1] = [23,59,59]
    if not len(days):
        days=list(range(1,31))
    if not len(prefPhase):
        prefPhase = ["A","C","D"]
    
    #removing days larger than 30
    Del=0
    for i in range(len(days)):
        i-=Del
        if days[i]>30:
            days.remove(days[i])
            Del+=1
    #converting elements in "days" to strings:
    for i in range(len(days)):
        if days[i] < 10:
            days[i] = "0"+str(days[i])
        else:
            days[i] = str(days[i])
    
    #converting user-specified end time to string:
    strt2 = ""
    for t in range(3):
        if times[1][t] < 10:
            strt2 += "0"+str(times[1][t])
        else:
            strt2 += str(times[1][t])
    #converting days to dates
    dates = []
    for i in range(len(days)):
        dates.append("201804" + days[i])
        
    nac, nICAO, nFlight = len(ac), len(prefICAO), len(prefFlight)

    expfolder = "./exportedData/"
    acfold = os.listdir(expfolder)
    for name in acfold:     #change to remove multiple files with a different file extension, from acfold
        if ".csv" in name:
            acfold.remove(name)
    print("\nFolders in \"exportedData\"")
    print(acfold)

    #remove folder names in "acfold" that do not meet user's specifications
    Del=0
    for i in range(len(acfold)):
        i-=Del
        if nac:         #unspecified a/c model -> all models
            if acfold[i] not in ac:
                acfold.remove(acfold[i])
                Del+=1

    acData = []
    for date in dates:      #dates are to be sorted in ascending order, store the dataframes for
                            #different dates as different list items in "acData"
        df2 = [["A",1], ["B",1], ["C",1]]
        pti = {"A":0, "C":1, "D":2}
        for elem in acfold:
            icaos = []  #stores all the desired ICAO directories
            ImportICAOs=[]
            for i in os.listdir(expfolder+elem+"/"):        
                if nICAO:
                    if i in prefICAO:
                        icaos.append(expfolder+elem+"/"+i+"/")
                        ImportICAOs.append(importDir+elem +"/"+i+"/")
                else:  #unspecified ICAO -> all ICAOs
                    icaos.append(expfolder+elem+"/"+i+"/")
                    ImportICAOs.append(importDir+elem +"/"+i+"/")
                if not os.path.exists(importDir+elem+"/" +i+"/"):
                    os.makedirs(importDir+elem+"/" +i+"/")
            #print("\n.csv files of a/c with user-specified ICAOs  (within", elem, "):\n")
            for i in range(len(icaos)):
                files = os.listdir(icaos[i])
                Del=0
                for j in range(len(files)):
                    j-=Del                    
                    if (".csv" not in files[j]) or ( int(files[j][9:15]) > int(strt2) ):     #remove non-csv filenames and filenames w/ a start time greater than user-specified interval, from "files"
                        files.remove(files[j])
                        Del+=1
                        continue
                    if date not in files[j]:        #remove filenames that don't correspond to current user-specified date
                        files.remove(files[j])
                        Del+=1
                        continue
                    if files[j][-5] not in prefPhase:
                        files.remove(files[j])
                        Del+=1
                        continue
                #print(files)   #this is a list of all csv files in a particular icao directory
                for name in files:
                    p = name[-5]
                    if nFlight:     #Conditional file read(user has specified a set of callsigns)
                        if name[16:-6] in prefFlight:    #account for change in file names after merging of branches
                            #print("Reading file:", name)
                            
                            #converting user-specified time intervals to unix
                            dt1 = datetime.datetime(2018, 4, int(days[dates.index(date)]), times[0][0], times[0][1], times[0][2] )
                            dt1 = time.mktime(dt1.timetuple())
                            dt2 = datetime.datetime(2018, 4, int(days[dates.index(date)]), times[1][0], times[1][1], times[1][2] )
                            dt2 = time.mktime(dt2.timetuple())

                            df = pd.read_csv(icaos[i]+name, header = 0, dtype={'icao':'str'})
                            df= MachNumber(df)
                            df = df.loc[(df["tc"].isin(typeCodes)) & (df["unix"] < dt2) & (df["unix"] > dt1)]
                            df = dfTimeToNormal(df)
                            #print(df[["unix", "icao", "tc"]])
                            #df.to_csv(ImportICAOs[i]+name) 
                            if isinstance(df2[pti[p]][1], int):
                                df2[pti[p]][1] = df    #df2[pti[p]][1] = (phase, df)
                            else:
                                df2[pti[p]][1] = df2[pti[p]][1].append(df)
                    else:           #Unconditional file read
                        #print("\nReading file:", name)
                        
                        #converting user-specified time intervals to unix
                        dt1 = datetime.datetime(2018, 4, int(days[dates.index(date)]), times[0][0], times[0][1], times[0][2] )
                        dt1 = time.mktime(dt1.timetuple())
                        dt2 = datetime.datetime(2018, 4, int(days[dates.index(date)]), times[1][0], times[1][1], times[1][2] )
                        dt2 = time.mktime(dt2.timetuple())
                        df = pd.read_csv(icaos[i]+name, header = 0, dtype={'icao':'str'})
                        #print(df)
                        df= MachNumber(df)
                        df = df.loc[(df["tc"].isin(typeCodes)) & (df["unix"] < dt2) & (df["unix"] > dt1)]
                        df = dfTimeToNormal(df)
                        #print(df[["unix", "icao", "tc"]])
                        #df.to_csv(ImportICAOs[i]+name) 
                        if isinstance(df2[pti[p]][1], int):
                            df2[pti[p]][1] = df
                        else:
                            df2[pti[p]][1] = df2[pti[p]][1].append(df)            
                #print("\n")
                
        for i in range(3):
            if isinstance(df2[i][1], int):
                df2[i][1] = "Null"
            else:
                df2[i][1] = df2[i][1].reset_index()
        acData.append(df2)
    return acData

Days.sort()
importedData = importData(aircraft,TypeCodes,Days,Times,PrefICAO,PrefFlight,PrefPhase)

print("-------------------------------------")
"""print("final dataframes:\n")
for i in range(len(importedData)):
    print("date:", str(Days[i])+"/04/2018:\n")
    for j in range(3):
        if not isinstance(importedData[i][j][1], str):
            importedData[i][j][1].sort_values("unix")
            print("Flight Phase:", importedData[i][j][0])
            print(importedData[i][j][1])
        else:
            print("Flight Phase:", importedData[i][j][0])
            print(importedData[i][j][1])"""
#exportedData --> acType --> ICAO --> flight files w/ dates, times, callsigns & flightphase"""

#importedData conatins info on all a/c that were specified by user. Each element is a database for one day
#---------------------------------------Analysis of Data--------------------------
aircraftDBfile = "aircraft_db.csv"
names = ['icao','regid','mdl','type','operator']
dtypes = {'icao':'str','regid':'str','mdl':'str','type':'str','operator':'str'}
aircraftDB = pd.read_csv(aircraftDBfile, header=0,names=names,dtype=dtypes)
aircraftDB.loc[:, "icao"] = aircraftDB.loc[:, "icao"].astype(str).str.upper()

#Highest performance parameter analysis:
if __name__ == "__main__":
    maxaltA = maxspA = maxrocA = 0
    maxaltC = maxspC = maxrocC= 0
    maxaltD = maxspD = maxrocD =0
    maxaltac = maxspac = maxrocac = ""
    maxaltacA = maxspacA = maxrocacA = ""
    maxaltacC = maxspacC = maxrocacC = ""
    maxaltacD = maxspacD = maxrocacD = ""

    print("\n")
    dfA_f=[]
    dfD_f=[]
    for i in tqdm(range(len(importedData))):
        print("Date:", str(Days[i])+"/04/2018")
        #Analyzing phase A
        if maxaltA < importedData[i][0][1].describe().loc["max","alt"]:
            maxaltA = importedData[i][0][1].describe().loc["max","alt"]
            maxaltacA = importedData[i][0][1].loc[importedData[i][0][1]["alt"] == maxaltA]["icao"].unique().astype(str)       #list of icaos of a/c with highest altitude
            for j in range(len(maxaltacA)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxaltacA[j] ]["mdl"].unique()):
                    maxaltacA[j] = aircraftDB.loc[aircraftDB["icao"] == maxaltacA[j]]["mdl"].unique()[0]
                else:
                    print(maxaltacA[j])
            maxaltacA = list(dict.fromkeys(maxaltacA))

        if (maxspA < importedData[i][0][1].describe().loc["max","speed"]) and (importedData[i][0][1].loc[importedData[i][0][1]["speed"] == importedData[i][0][1].describe().loc["max","speed"]]["Mach"] < 1.1).all():
            maxspA = importedData[i][0][1].describe().loc["max","speed"]
            maxspacA = importedData[i][0][1].loc[importedData[i][0][1]["speed"] == maxspA]["icao"].unique().astype(str)       #list of icaos of a/c with highest cruise speed
            for j in range(len(maxspacA)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxspacA[j] ]["mdl"].unique()):
                    maxspacA[j] = aircraftDB.loc[ aircraftDB["icao"] == maxspacA[j] ]["mdl"].unique()[0]
                else:
                    print(maxspacA[j])
            maxspacA = list(dict.fromkeys(maxspacA))
        
        if (abs(maxrocA) < abs(importedData[i][0][1].describe().loc["max","climb"])) and (abs(importedData[i][0][1].describe().loc["max","climb"]) < 9000.):
            maxrocA = importedData[i][0][1].describe().loc["max","climb"]
            maxrocacA = importedData[i][0][1].loc[importedData[i][0][1]["climb"] == maxrocA]["icao"].unique().astype(str)       #list of icaos of a/c with highest climbrate
            for j in range(len(maxrocacA)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxrocacA[j] ]["mdl"].unique()):
                    maxrocacA[j] = aircraftDB.loc[aircraftDB["icao"] == maxrocacA[j]]["mdl"].unique()[0]
                else:
                    print(maxrocacA[j])
            maxrocacA = list(dict.fromkeys(maxrocacA))

        #Analyzing Phase C
        if maxaltC < importedData[i][1][1].describe().loc["max","alt"]:
            maxaltC = importedData[i][1][1].describe().loc["max","alt"]
            maxaltacC = importedData[i][1][1].loc[importedData[i][1][1]["alt"] == maxaltC]["icao"].unique().astype(str)       #list of icaos of a/c with highest altitude
            for j in range(len(maxaltacC)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxaltacC[j] ]["mdl"].unique()):
                    maxaltacC[j] = aircraftDB.loc[aircraftDB["icao"] == maxaltacC[j]]["mdl"].unique()[0]
                else:
                    print(maxaltacC[j])
            maxaltacC = list(dict.fromkeys(maxaltacC))

        if maxspC < importedData[i][1][1].describe().loc["max","speed"] and (importedData[i][1][1].loc[importedData[i][1][1]["speed"] == importedData[i][1][1].describe().loc["max","speed"]]["Mach"] < 1.1).all():
            maxspC = importedData[i][1][1].describe().loc["max","speed"]
            maxspacC = importedData[i][1][1].loc[importedData[i][1][1]["speed"] == maxspC]["icao"].unique().astype(str)       #list of icaos of a/c with highest cruise speed
            for j in range(len(maxspacC)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxspacC[j] ]["mdl"].unique()):
                    maxspacC[j] = aircraftDB.loc[ aircraftDB["icao"] == maxspacC[j] ]["mdl"].unique()[0]
                else:
                    print(maxspacC[j])
            maxspacC = list(dict.fromkeys(maxspacC))
        
        if (abs(maxrocC) < abs(importedData[i][1][1].describe().loc["max","climb"])) and (abs(importedData[i][1][1].describe().loc["max","climb"]) < 9000.):
            maxrocC = importedData[i][1][1].describe().loc["max","climb"]
            maxrocacC = importedData[i][1][1].loc[importedData[i][1][1]["climb"] == maxrocC]["icao"].unique().astype(str)       #list of icaos of a/c with highest climbrate
            for j in range(len(maxrocacC)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxrocacC[j] ]["mdl"].unique()):
                    maxrocacC[j] = aircraftDB.loc[aircraftDB["icao"] == maxrocacC[j]]["mdl"].unique()[0]
                else:
                    print(maxrocacC[j])
            maxrocacC = list(dict.fromkeys(maxrocacC))
        



        #Analyzing Phase D
        if maxaltD < importedData[i][2][1].describe().loc["max","alt"]:
            maxaltD = importedData[i][2][1].describe().loc["max","alt"]
            maxaltacD = importedData[i][2][1].loc[importedData[i][2][1]["alt"] == maxaltD]["icao"].unique().astype(str)       #list of icaos of a/c with highest altitude
            for j in range(len(maxaltacD)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxaltacD[j] ]["mdl"].unique()):
                    maxaltacD[j] = aircraftDB.loc[aircraftDB["icao"] == maxaltacD[j]]["mdl"].unique()[0]
                else:
                    print(maxaltacD[j])
            maxaltacD = list(dict.fromkeys(maxaltacD))

        if maxspD < importedData[i][2][1].describe().loc["max","speed"] and (importedData[i][2][1].loc[importedData[i][2][1]["speed"] == importedData[i][2][1].describe().loc["max","speed"]]["Mach"] < 1.1).all():
            maxspD = importedData[i][2][1].describe().loc["max","speed"]
            maxspacD = importedData[i][2][1].loc[importedData[i][2][1]["speed"] == maxspD]["icao"].unique().astype(str)       #list of icaos of a/c with highest cruise speed
            for j in range(len(maxspacD)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxspacD[j] ]["mdl"].unique()):
                    maxspacD[j] = aircraftDB.loc[ aircraftDB["icao"] == maxspacD[j] ]["mdl"].unique()[0]
                else:
                    print(maxspacD[j])
            maxspacD = list(dict.fromkeys(maxspacD))
        
        if (abs(maxrocD) < abs(importedData[i][2][1].describe().loc["min","climb"])) and (abs(importedData[i][2][1].describe().loc["min","climb"]) < 9000.):
            maxrocD = importedData[i][2][1].describe().loc["min","climb"]
            maxrocacD = importedData[i][2][1].loc[importedData[i][2][1]["climb"] == maxrocD]["icao"].unique().astype(str)       #list of icaos of a/c with highest climbrate
            for j in range(len(maxrocacD)):
                if len(aircraftDB.loc[ aircraftDB["icao"] == maxrocacD[j] ]["mdl"].unique()):
                    maxrocacD[j] = aircraftDB.loc[aircraftDB["icao"] == maxrocacD[j]]["mdl"].unique()[0]
                else:
                    print(maxrocacD[j])
            maxrocacD = list(dict.fromkeys(maxrocacD))

        """
        #Analyzing range to top of climb
        DATA = importedData[i][0][1]
        ICAOSNeeded1= DATA.icao.unique()
        dfA = pd.DataFrame(columns={"ICAO":"str","Range(toc) [ft]":"float"})
        dfA["ICAO"] = ICAOSNeeded1
        for j in range (len(ICAOSNeeded1)):
            DataNeeded = DATA.loc[DATA["icao"]== ICAOSNeeded1[j]]
            dfA.loc[j,"Range(toc) [ft]"] = Range(DataNeeded,DataNeeded.first_valid_index())
        dfA = dfA.convert_dtypes()
        dfA_f.append(dfA)
        
        #Analyzing range to bottom of descent
        DATA = importedData[i][2][1]
        ICAOSNeeded2= DATA.icao.unique()
        dfD = pd.DataFrame(columns={"ICAO":"str","Range(bod) [ft]":"float"})
        dfD["ICAO"] = ICAOSNeeded2
        for j in range (len(ICAOSNeeded2)):
            DataNeeded = DATA.loc[DATA["icao"]== ICAOSNeeded2[j]]
            dfD.loc[j, "Range(bod) [ft]"] = Range(DataNeeded,DataNeeded.first_valid_index())
            #print("Range to bottom of descent for ", ICAOSNeeded2[j] ,"=", RangeClimb ,"ft")
        dfD = dfD.convert_dtypes()
        dfD_f.append(dfD)"""
    
    print(maxaltacA, "max. altitude in ascent:",maxaltA, "[ft]")
    print(maxspacA, "max. airspeed in ascent:",maxspA, "[kts]")
    print(maxrocacA, "max. roc in ascent:",maxrocA, "[ft/min]")
    print()

    print(maxaltacC, "max. altitude in Cruise:",maxaltC, "[ft]")
    print(maxspacC, "max. airspeed in Cruise:",maxspC, "[kts]")
    print(maxrocacC, "max. roc in Cruise:",maxrocC, "[ft/min]")
    print()
    
    print(maxaltacD, "max. altitude in descent:",maxaltD, "[ft]")
    print(maxspacD, "max. airspeed in descent:",maxspD, "[kts]")
    print(maxrocacD, "max. roc in descent:",maxrocD, "[ft/min]")
    print()

    """print(dfA_f)
    print()
    print(dfD_f)"""
