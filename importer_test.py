import pandas as pd
import numpy as np
import pyModeS as pms
import datetime
import time
from exporter import dfTimeToNormal


"""
Note from Bhargava:
This file is used as a reference for the file "importer_test.py"
This is merely meant to give an idea of the output dataframe for some simple 
importing parameters would be, and uses the file "dh8d_20180401.csv"
"""

#Parameters of flights to import
#A blank parameter indicates a lack of preference
acType = ["dh8d"]   #check a/c database for model name(s)
tcGroup = [2]
spDays = ["01"]
timeInt = [[9,57,11],[10, 0, 30]]

def importData(
    aircraftType,      
    typeCodes, # Default: All TCs (see expample 3 below)
    days,  # 1st-30th of April
    times                # 0h-24h - hours of the day
    ):
    """[imports data from the exportedData folder and applies filters]
    
    Arguments (filters):
        [aircraftType {list of strings}] -- [Aircraft Type] 

        [typecodes {list of int}] -- [allow users to load only POS or only VEL or both]
    
        [days {list of strings}] -- [string referring to days of April 2018]
        
        [times {2D list of int}] -- [ start and end time [[hrs1, mins1, secs1] , [hrs2, mins2, secs2]] ]
    Returns
        [acData {pandas.DataFrame}] -- [Data imported based on conditions]
        #I hope to make this a list of dataframes, each list item producing a dataframe
        #that corresponds to a single date, arranged in ascending order of date.

        Daniel's Note:   the output DataFrame may need to be nested to mimic the exportedData folder structure
    """
    #setting default values
    if not len(typeCodes):
        typeCodes = [1,2,3,4]
    if not len(times[0]):
        times[0] = [0,0,0]
    if not len(times[1]):
        times[1] = [23,59,59]
    if not len(days):
        days=[range(1,31)]  #convert to string later

    dt1 = datetime.datetime(2018, 4, int(days[0]), times[0][0], times[0][1], times[0][2] )
    dt1 = time.mktime(dt1.timetuple())
    dt2 = datetime.datetime(2018, 4, int(days[0]), times[1][0], times[1][1], times[1][2] )
    dt2 = time.mktime(dt2.timetuple())

    date = []
    for i in range(len(days)):
        date.append("201804" + days[i])
    
    expfolder = "./exportedData/"

    #--------------make changes from here------------------
    fileName = expfolder+aircraftType[0]+"_"+date[0]+".csv"
    #fileName = expfolder+aircraftType[0]+"_"+"flights"+"/"+dates
    print("\nretrieving data from", fileName, "\n")
    #--------------to here --------------------------------
    
    #--------------no changes from here-------------------
    acData = pd.read_csv(fileName, header = 0)
    #exportedData -> acType -> icao -> flights

    acData = acData.loc[(acData["tc"].isin(typeCodes)) & (acData["unix"] < dt2) & (acData["unix"] > dt1)]
    print("Check if fromatting conditions are met")
    acData = dfTimeToNormal(acData)
    print(acData[["unix", "icao", "tc"]])
    #----------------to here --------------------------------

    return acData

importData(acType, tcGroup, spDays, timeInt)
