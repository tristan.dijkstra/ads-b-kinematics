from math import *
TValues=[288.15]
Counter=0
T0=TValues[Counter]
ZeroVal=0
def ISA(h):
    Counter = 0
    hValues = [11000,20000,32000,47000,51000,71000,86000,100000]
    aValues=[-0.0065,0,0.0010,0.0028,0,-0.0028,-0.0020,0]
    PValues=[101325]
    DValues=[1.225]
    TValues=[288.15]
    T0=TValues[0]
    LayerBoundary = hValues[Counter]
    MaxLayer= Counter+1
    MinLayer = Counter -1
    a = aValues[Counter]
    g0 = 9.80665
    R = 287
    P0 = PValues[Counter]
    Exponent = -g0/(a*R)
    Exponent1 = -g0/(aValues[Counter]*R)
    if h<= 11000:
       T1 = T0 + (aValues[0]*h)
       P1 = P0 * ((T1/T0)**Exponent)
       D1= P1/(R*T1)
    else:
        while LayerBoundary < h:
            if Counter==0:
                ZeroVal=0
            else:
                ZeroVal=hValues[MinLayer]
            if aValues[Counter] != 0:
                Exponent1 = -g0/(aValues[Counter]*R)
                T1 = TValues[Counter] + (aValues[Counter]*(LayerBoundary-ZeroVal))
                TRatio = T1/TValues[Counter]
                P1 = PValues[Counter] * (TRatio**(Exponent1))
                D1= P1/(R*T1)
            elif h< hValues[Counter]:
                ExpIsotherm = -(g0*(h-LayerBoundary))/(R*TValues[Counter])
                P1 = PValues[Counter] *exp(ExpIsotherm)
                D1 = P1/(R*TValues[Counter])
                T1= TValues[Counter]
            elif h > hValues[Counter]:
                ExpIsotherm = -(g0*(LayerBoundary-ZeroVal))/(R*TValues[Counter])
                P1 = PValues[Counter] *exp(ExpIsotherm)
                D1 = P1/(R*TValues[Counter])
                T1= TValues[Counter]
            PValues.append(P1)
            DValues.append(D1)
            TValues.append(T1)
            Counter = Counter + 1
            LayerBoundary = hValues[Counter]
            MaxLayer=MaxLayer+1
            MinLayer= MinLayer+1
        if aValues[Counter]> 0 or aValues[Counter] <0:
            T1 = TValues[Counter] + (aValues[Counter]*(h-hValues[MinLayer]))
            TRatio= T1/TValues[Counter]
            Exponent1=(-g0)/(aValues[Counter]*287)
            P1 = PValues[Counter] * (TRatio**Exponent1)
            D1= P1/(R*T1)
        else:
            ExpIsotherm = -(g0*(h-hValues[MinLayer]))/(R*TValues[Counter])
            P1 = PValues[Counter] *exp(ExpIsotherm)
            D1 = P1/(R*TValues[Counter])
            T1= TValues[Counter]
    FinalValues=[D1,P1,T1]
    return (FinalValues)

##print(ISA(10668))
