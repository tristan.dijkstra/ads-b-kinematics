import os
import numpy as np
import pyModeS as pms
from pyModeS.decoder.bds.bds05 import altitude,airborne_position,airborne_position_with_ref
from pyModeS.decoder.bds.bds08 import callsign
from pyModeS.decoder.bds.bds06 import surface_position,surface_position_with_ref,surface_velocity
from pyModeS.decoder.bds.bds09 import airborne_velocity,altitude_diff
from pyModeS import common
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
from tqdm import tqdm as tqdm

exportDir = "exportedData/"
if not os.path.exists(exportDir):
    os.makedirs(exportDir)

def getAircraftDataByMDL(aircraftMDL, originalData):
    '''
    Gets all aircraft data on a day for a certain aircraft mdl
    Takes aircraftMDL and the full day's dataframe as input
    Returns only the data for a full day of the aircraft specified
    '''
    aircraftDB = pd.read_csv(
        './aircraftDB/{}.csv'.format(aircraftMDL), 
        header=0,
        names=['icao','regid','mdl','type','operator'],
        dtype={'icao':'str','regid':'str','mdl':'str','type':'str','operator':'str'}
        )
    aircraftDB.loc[:, "icao"] = aircraftDB.loc[:, "icao"].astype(str).str.upper()
    return originalData.loc[originalData['icao'].isin(aircraftDB["icao"])]


def dfTimeToNormal(inputData):
    def timeFromUnix(unixtime):
        '''Unix to readable date'''
        return datetime.fromtimestamp(unixtime).time()
    inputData.update(inputData.loc[:, 'unix'].apply(timeFromUnix))
    return inputData


def s_position(msg0, msg1, t0, t1, lat_ref=51.989884, lon_ref=4.375374):
    # the surface-position function, but with the receiver-position fixed in place
    return surface_position(msg0, msg1, t0, t1, lat_ref, lon_ref)

def getPos(t0,t1,m0,m1,e0,e1,speed,lat0,lon0,func=airborne_position,funcREF=airborne_position_with_ref):
    if not pd.isnull(lat0) and abs(t1-t0) < 1000: # only valid if the distance travelled is <180NM
        return funcREF(m1,lat0,lon0)
    elif e0 != e1 and abs(t1-t0) < 10:
        pos = func(m0,m1,t0,t1)
        if pos is not None:
            return pos[0],pos[1]
    return np.nan, np.nan # If none of the conditions are met, return NaN. These NaN values are later recovered by backtrackNaN()


def backtrackNaN(inputData,indexes,lat1,lon1,t1):
    latX,lonX,tX0 = lat1,lon1,t1
    for j in reversed(indexes): # add 180NM constraint
        tX = inputData.at[j,'unix']
        if abs(tX-tX0) < 1000:
            latX,lonX = airborne_position_with_ref(inputData.at[j,'message'],latX,lonX)
            inputData.at[j,'lon'] = lonX
            inputData.at[j,'lat'] = latX
            tX0 = tX
        else: break 

def getFlightPhase(parameter,threshold=400):
    if parameter > threshold:           return "A" # ascent
    elif abs(parameter) <= threshold:   return "C" # cruise
    elif parameter < -threshold:        return "D" # descent
    return "X" # unknown (i.e. "parameter" is NaN)
    
def slope(a1,a0,t1,t0,multiplier=60): 
    # the multiplier of 60 is for our specific case where we go from feet/s to feet/min
    if pd.isnull(a0) or pd.isnull(t0):
        return np.nan
    return multiplier*(a1-a0)/(t1-t0)

def convertMessages(inputData, aircraftMDL, dataDate="", plotFlights=False, printInfo=False, nanBackTracking=True):
    """[converts raw messages in ADS-B dataframes with correct data]
    
    Arguments:
        inputData {[pandas Dataframe]} -- [Raw ADS-B dataframe]
        nanBackTracking {bool}         -- [replace NaN-values in position data by backtracking from next known position]
    
    Returns
        [convertedData] -- [Data with messages converted]
    """
    # create the export folder
    exportDir = "{}/exportedData/{}/".format(os.path.dirname(__file__),aircraftMDL)
    if not os.path.exists(exportDir):
        os.makedirs(exportDir)

    # sorting first by ICAO, then by timestamp
    # then reset the index so it's 0,1,2,3.. again
    inputData = inputData.sort_values(by=['icao', 'unix']).reset_index(drop=True).copy()

    lastCutIndex         = inputData.index[0]
    T0=M0=E0             = np.nan # "previous row" parameters
    Talt0                = np.nan
    icao0                = True 
    callsign0=callsign1  = np.nan 
    lat0=lon0=alt0       = np.nan # Reference position for "with_ref" functions
    speed=heading=climb  = np.nan # Reference values for max-distance check
    nanIndexes = list() # For keeping track of position messages which couldn't be decoded
    saveFig = False                 # later set to if and when a graph needs to be saved and cleared

    # Parameters for flight-phase determination
    stableClimbRate = 100       # [feet per minute] +/-climb rate
    timeWindow = 18             # max window around each datapoint to analyse for changes in flight phase
    pastValues = list()         # stores previous datapoints within a window (window size set above)
    cutCondition = False        # later set to True whenever a cut needs to be made
    wasSteady = True            # a latch to prevent unnecessary cuts right after a legitimate cut
    twoCuts = False
    firstAfterCut = True        # set to False after the first altitude-datapoint is stored for later analysis
    flightPhaseCut = False
    TBeginning=altBeginning = np.nan # used to determine the overall slope of each flight-phase 
    flightPhase=previousPhase=tempFlightPhase = 'X' # later set to 'A','C' and 'D' - 'X' for unknown
    preCLR = np.nan

    for i in inputData.index: 
        tc = inputData.at[i,'tc']
        T1 = inputData.at[i,'unix']
        dt = T1-T0
        M1 = inputData.at[i,'message']
        E1 = pms.adsb.oe_flag(M1)
        icao1 = inputData.at[i,'icao']

        boolVelocity    = bool(tc == 19)
        boolAirbornePos = bool((9 <= tc <= 22) and not boolVelocity)
        boolSurfacePos  = False # bool(5 <= tc <= 8) # surface TCs no longer loaded
        boolCallsign    = bool(1 <= tc <= 4)

        if boolVelocity:
            aVelocity = airborne_velocity(M1)
            # some "velocity" messages return None for reasons I don't know yet. This seems to be very rare, so for now I'm just skipping them.
            if aVelocity is not None:
                speed,heading,climb = aVelocity[0:3]
                inputData.at[i,'speed']   = speed
                inputData.at[i,'heading'] = heading
                inputData.at[i,'climb']   = climb
            
        elif boolAirbornePos: 
            lat1,lon1 = getPos(
                T0,T1,
                M0,M1,
                E0,E1,
                speed,
                lat0,lon0
                )
            alt1 = altitude(M1)

            climb = slope(alt1,alt0,T1,Talt0) # not stored in the database, only used for kink detection
            Talt0 = T1

            inputData.at[i,'alt']=alt0 = alt1
            inputData.at[i,'lon']=lon0 = lon1
            inputData.at[i,'lat']=lat0 = lat1
            if firstAfterCut: # first altitude datapoint of this flight phase for later A,C,D determination
                TBeginning,altBeginning = T1,alt1
                firstAfterCut = False 
            
        elif boolSurfacePos: # Surface messages contain speed, heading, latitude and longitude
            sVelocity = surface_velocity(M1)
            # some "velocity" messages return None for reasons I don't know yet. This seems to be very rare, so for now I'm just skipping them.
            if sVelocity is not None:
                speed,heading = sVelocity[0:2]
                inputData.at[i,'speed']   = speed
                inputData.at[i,'heading'] = heading

            lat1,lon1 = getPos(
                T0,T1,
                M0,M1,
                E0,E1,
                speed,
                lat0,lon0,
                func=s_position,
                funcREF=surface_position_with_ref
                )

            inputData.at[i,'lon']=lon0 = lon1
            inputData.at[i,'lat']=lat0 = lat1

        elif boolCallsign: # Callsign
            callsign1 = callsign(M1).replace('_','')

        if (boolVelocity or boolAirbornePos) and climb is not None:
            # we save our climb-rate to a list in order 
            pastValues.append(climb)
            nPastValues = len(pastValues)

            # keep only the values that are within the specified window
            while nPastValues > timeWindow: 
                del(pastValues[0])
                nPastValues = len(pastValues)

            if nPastValues > 4: # consider a cut only when there are more than this many datapoints
                # split the window into three sub-windows and get each window's median
                subWindow = int(nPastValues/3)
                preCLR = np.median(pastValues[:subWindow])
                datumCLR = np.median(pastValues[subWindow:-subWindow])
                postCLR = np.median(pastValues[-subWindow:])

                # for each sub-window, check whether the median-climb-rate is within the "stable" limit
                boolPreCLR      = bool(abs(preCLR) < stableClimbRate)
                boolDatumCLR    = bool(abs(datumCLR) < stableClimbRate)
                boolPostCLR     = bool(abs(postCLR) < stableClimbRate)
                # 'kinkDetected' == True only if ((False,True,True) or (True,False,False))
                #  i.e. the previous median-climb-rate is unequal to both current and future median-climb-rates
                kinkDetected    = ((boolPreCLR != boolDatumCLR) and (boolPreCLR != boolPostCLR))

                if wasSteady and kinkDetected:
                    # we will cut in the middle of the window
                    datumPoint = int(nPastValues/2) 
                    
                    tempFlightPhase = getFlightPhase(preCLR)
                    
                    if tempFlightPhase == previousPhase or previousPhase == 'X':
                        tempCutIndex = i-datumPoint

                    elif tempFlightPhase != previousPhase:
                        cutIndex        = tempCutIndex
                        flightPhase     = previousPhase
                        tempCutIndex    = i-datumPoint
                        cutCondition    = True
                        flightPhaseCut  = True
                        wasSteady       = False
                    previousPhase   = tempFlightPhase

                    tempCutIndex        = i-datumPoint
                # 'wasSteady' acts as a latch to prevent unnecessary cuts as the next few datapoints may still trigger the 'kinkDetected'
                elif not kinkDetected:
                    wasSteady = True

        # back-tracking on NaN-position-values
        # The first few positions are always NaN until we get a fix with an even/odd pair
        # Once we have a fix, we can use it as a reference to back-track the previous positions
        if (boolAirbornePos or boolSurfacePos) and nanBackTracking:
            if pd.isnull(lat1):
                nanIndexes.append(i)
            elif pd.isnull(lat0) and not pd.isnull(lat1):
                backtrackNaN(inputData,nanIndexes,lat1,lon1,T1)
                nanIndexes = list()




        # Cutting Process starts here

        # a change in ICAO requires a cut, but it also requires the graphs to be saved (if plotting is enabled)
        boolICAO =     bool(icao0 != icao1) # A change in icao is a pretty obvious sign of separate flights
        negativeTime = bool(dt < 0) # due to the way the data is sorted, negative dt must be another flight)
        overTime =     bool(dt > 1000) # max time in seconds between messages

        globalCutConditions = any([
            negativeTime,
            overTime,
            boolICAO, 
            ])


        # condStr = ''
        # try:
        #     if not wasSteady:       condStr += 'unsteady '
        #     if kinkDetected:    condStr += 'kink '+str(cutIndex)
        # except:
        #     pass
        # if negativeTime:    condStr += 'neg '
        # if overTime:        condStr += 'over '
        # if boolICAO:        condStr += 'icao '
        # try: clrstr = ','.join([str(round(preCLR,0)),str(round(datumCLR,0)),str(round(postCLR,0))])
        # except: clrstr = '_-_-'

        # debugInfo = '\t'.join([str(i),str(icao1),str(callsign1),str(round(alt1,0)),str(round(climb,0)),str(nPastValues),str(clrstr),previousPhase,flightPhase,tempFlightPhase,str(condStr)])
        # x = input(debugInfo)
        

        if plotFlights and boolICAO:
            saveFig = True

        # Checking whether we need to cut here for any reason OTHER than a change in flight phase
        # Flight phase cuts are checked for separately above!
        twoCuts = False
        if globalCutConditions:
            cutIndex = i # cut right here
            if not pd.isnull(preCLR):
                cutCondition = True
                flightPhase = getFlightPhase(preCLR)
                if flightPhaseCut:
                    twoCuts = True
                    firstCutIndex        = tempCutIndex
                    firstFlightPhase     = previousPhase
                flightPhaseCut = False

            # resetting some values that shouldn't be carried over to the next flight (they may only be carried over between phases of a single flight)
            previousPhase = 'X' 
            pastValues = list()
        
        if cutCondition:
            if twoCuts:
                cut(inputData.iloc[lastCutIndex:firstCutIndex].copy(),
                    exportDir,      # the file path to save on. Carried over from main function
                    dataDate,       # the date for the file title. Carried over from main function
                    icao0,          # ICAO from before the cut (in case it changed)
                    callsign0,      # callsign from before the cut (in case it changed)
                    firstFlightPhase,    # flightPhase string for file name
                    plotFlights,    # plot yes/no. Carried over from main function
                    False,        
                    printInfo       # print status info yes/no. Carried over from main function
                )
                lastCutIndex = firstCutIndex
            cut(
                    inputData.iloc[lastCutIndex:cutIndex].copy(), # our DataFrame since the previous cut
                    exportDir,      # the file path to save on. Carried over from main function
                    dataDate,       # the date for the file title. Carried over from main function
                    icao0,          # ICAO from before the cut (in case it changed)
                    callsign0,      # callsign from before the cut (in case it changed)
                    flightPhase,    # flightPhase string for file name
                    plotFlights,    # plot yes/no. Carried over from main function
                    saveFig,        # True whenever the ICAO has changed -> saves the graph and clears it for the next aircraft
                    printInfo       # print status info yes/no. Carried over from main function
                    )

            preCLR = np.nan
            lastCutIndex = cutIndex  
            firstAfterCut = True    # used to save the first altitude-datapoint of the next phase
            cutCondition = False
            saveFig = False

        # At the end of the loop, the 'current' message becomes next loop's 'previous' message
        T0,M0,E0,icao0,callsign0 = T1,M1,E1,icao1,callsign1
    


def cut(
            exportData,
            exportDir,
            dataDate,
            icao,
            callsign,
            flightPhase,
            plotFlights=False,
            saveFig=False,
            printInfo=False
            ):
    try:
        time = str(datetime.fromtimestamp(exportData.at[exportData.index[0],'unix']).time())[0:8].replace(":", "")
        
        icaoDir = '{}{}/'.format(exportDir,icao)
        if not os.path.exists(icaoDir):
            os.makedirs(icaoDir)

        fileName = "{}_{}_{}_{}.csv".format(dataDate,time,callsign,flightPhase)
        if printInfo:
            print("Saved: {} {}".format(icao,fileName))

        exportData = exportData.loc[exportData.tc > 4].copy()
        exportData.drop('message',axis=1,inplace=True)
        
        exportData.to_csv(icaoDir+fileName,index=False)

        if plotFlights:
            exportData = exportData[exportData.alt.notnull()]
            #exportData = dfTimeToNormal(exportData)
            plt.plot(
                exportData["unix"],
                exportData["alt"],
                marker="o")

            firstPoint = exportData.index[0]    
            plt.annotate(flightPhase,(exportData.at[firstPoint,"unix"],exportData.at[firstPoint,"alt"]))

            if saveFig:
                plt.title(icao+' '+dataDate)
                plt.savefig(icaoDir+icao+'_'+dataDate+".png")
                #plt.show()
                plt.clf()
    except IndexError:
        pass


def exportSingle(AircraftMDL, dataFile, printOut=False, plotFlights=False):
    dateIndex = dataFile.index('.csv')
    dataDate = dataFile[dateIndex-8:dateIndex] # the last 8 digits in the file name before .csv
    rawdata = pd.read_csv(
        dataFile,
        names=['unix', 'icao', 'tc', 'message','lat','lon','alt','speed','heading','climb'],
        dtype={'unix':'float','icao':'str','tc':'int','message':'str','lat':'float','lon':'float','alt':'float','speed':'float','SPDreferenceFrame':'str','heading':'float','climb':'float'}
        )
    
    
    # keeping only the messages with the correct type-codes
    aircraftData = getAircraftDataByMDL(AircraftMDL, rawdata.query('0<tc<5 or 8<tc<23')) 

    convertMessages(aircraftData,AircraftMDL,dataDate,plotFlights,printOut)


def exportMultiple(AircraftMDL, dataFolder, printOut=False, plotFlights=False):
    # print(os.listdir(dataFolder))
    # amountOfLines = 0
    # amountOfLinesConverted = 0
    for dataFile in tqdm(sorted(os.listdir(dataFolder))):
        dataFile = dataFolder + dataFile
        # print(dataFile)
        if printOut:
            print("Processing: " + dataFile)

        exportSingle(AircraftMDL, dataFile, printOut, plotFlights)