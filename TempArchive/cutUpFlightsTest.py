import numpy as np
import pyModeS as pms
import pandas as pd
import exporter
import matplotlib.pyplot as plt
import os
import datetime

def cutUpFlights(dataFile, plotFlights=False, printInfo=False):
    exportFolder = dataFile[0:-4] + "_flights/"
    plotFlights = True

    data = exporter.quickimport(dataFile)

    uniqueICAOs = data['icao'].unique()
    if printInfo:
        print("Unique ICAOs: " + str(len(uniqueICAOs)))

    for ICAOindex, currentICAO in enumerate(uniqueICAOs):
        if printInfo:
            print("Processing ICAO number " + str(ICAOindex + 1) + "/" + str(len(uniqueICAOs)) + ": " + currentICAO)
        currentICAOData = data.loc[data['icao'] == currentICAO]
        maxInterval = 1000 #seconds
        cuts = []
        for u in range(len(currentICAOData) - 1):
            currentRow = currentICAOData.iloc[u]
            nextRow = currentICAOData.iloc[u + 1] 
            timeDifference = nextRow["unix"] - currentRow["unix"]
            if timeDifference > maxInterval:
                cuts.append(currentRow)

        cutsDF = pd.DataFrame(cuts)
        cutsDFIndices = cutsDF.index.values.tolist()
        cutsDFIndices = [u+1 for u in cutsDFIndices]
        # adds the boundary indices, 0 and the last value
        cutsDFIndices_mod = [0] + cutsDFIndices + currentICAOData.tail(1).index.values.tolist()
        
        
        listOfCutDFs = []
        listOfCutDFsHeight = []
        # MAKE CUTS
        currentICAOFullData = data.loc[data['icao'] == currentICAO] # get back full data
        for u in range(len(cutsDFIndices_mod)-1):
            listOfCutDFs.append(currentICAOFullData.loc[cutsDFIndices_mod[u]:cutsDFIndices_mod[u+1]])
            # Append to additional list for plotting height data.
            if plotFlights:
                listOfCutDFsHeight.append(currentICAOData.loc[cutsDFIndices_mod[u]:cutsDFIndices_mod[u+1]])

        # FIND THE CALLSIGN
        listOfCallsigns = []
        for df in listOfCutDFs:
            currentCallsign = (df.loc[:, 'callsign'].dropna().unique()) # get a list with all callsigns
            if len(currentCallsign) > 0:
                listOfCallsigns.append(str(currentCallsign[0]).replace("_", ""))
            else:
                listOfCallsigns.append("NoCallsign")

        # SAVE THE INDIVIDUAL FLIGHTS
        ICAOfolder = currentICAO + "/"
        # Date info
        dateIndex = dataFile.index('.csv')
        dataDate = dataFile[dateIndex-8:dateIndex]
        # makes folder if it doesn't exist yet
        if not os.path.exists(exportFolder + ICAOfolder):
            os.makedirs(exportFolder + ICAOfolder)
        for index, df in enumerate(listOfCutDFs):
            # get time from first entry in flight df
            time = str(datetime.datetime.fromtimestamp(df.iloc[0, 0]).time())[0:5].replace(":", "")
            # Names the flight date_callsign_time(hh:mm)
            flightName = dataDate + "_" + time + "_" + listOfCallsigns[index]
            totalPath = exportFolder + ICAOfolder + flightName + ".csv"
            if printInfo:
                print("Saved: " + totalPath)
            df.to_csv(totalPath)

        # PLOT FLIGHTS FOR ICAO
        if plotFlights:
            # display height data and cuts
            plt.suptitle(currentICAO)
            colors = ["c", "g", "b", "m", "k", "y", "tab:pink", "tab:purple"]
            currentICAOFullHeightData = data.loc[data['icao'] == currentICAO]
            # plot the full data.
            plt.plot(currentICAOFullHeightData["unix"],currentICAOFullHeightData["alt"], color="tab:orange")
            # plot data of individual flights
            for u in range(len(listOfCutDFs)):
                plt.plot(listOfCutDFsHeight[u]["unix"], listOfCutDFsHeight[u]["alt"], marker=".", color=colors[u])
            if len(cutsDF) > 0:
                plt.scatter(cutsDF["unix"], cutsDF["alt"], marker= "D", color="r")
            plt.savefig(exportFolder + ICAOfolder + currentICAO + ".png")
            plt.clf()
