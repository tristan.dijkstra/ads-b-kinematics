import numpy as np
import pyModeS as pms
import pandas as pd

msg = "8D4840D6202CC371C32CE0576098"
msg = msg.lower()

# ICAO DATABASE
aircraftDBfile = "aircraft_db.csv"
aircraftDB = pd.read_csv(aircraftDBfile)
letterCodes = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ#####_###############0123456789######"

ICAO = msg[2:6+2]


msgMessage = msg[10:8+14] #The message part of the ADS-B
print("original message:" , msgMessage)
binary = pms.hex2bin(msgMessage)
letters = []
indx = 0
# splits it in 6 bit binary.
while indx < len(binary):
    letters.append(str(binary)[indx:indx+6])
    indx += 6

# converts binary to numbers and letters
convertedLetters = []
convertedDecimal = []
for letter in letters:
    l = pms.bin2int(letter)
    convertedDecimal.append(l)
    convertedLetters.append(letterCodes[l])
print(convertedDecimal)
print(convertedLetters)
finalMessage = "".join(convertedLetters)
print("Final Message: ", finalMessage)

searchResult = aircraftDB.loc[aircraftDB['icao'] == ICAO]
# df.loc[df['column_name'] != some_value]
print("ICAO data from Database: ")
print(searchResult)

print("hello")