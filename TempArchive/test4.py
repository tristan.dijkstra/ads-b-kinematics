import numpy as np
import pyModeS as pms
from pyModeS.decoder.bds.bds05 import altitude
from pyModeS.decoder.bds.bds08 import callsign
from pyModeS.decoder.bds.bds06 import surface_position, surface_velocity
from pyModeS.decoder.bds.bds09 import airborne_velocity
import pandas as pd
import exporter
import matplotlib.pyplot as plt

dataFile = "exportedData/e190_20180401_flights/4B19F4_1.csv"

data = exporter.quickimport(dataFile)
print(data)
data = exporter.dfTimeToNormal(data)

plt.plot(data["unix"], data["message"], marker=".")
plt.show()