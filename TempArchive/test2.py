import numpy as np
import pyModeS as pms
from pyModeS.decoder.bds.bds05 import altitude
import pandas as pd
import datetime
import matplotlib.pyplot as plt

ICAOchosen = "4A08E2"

# ICAO DATABASE
datafile = "ads-b-data/ADSB_RAW_20180401.csv"
data = pd.read_csv(datafile, nrows=10000000, names=['unix','icao','tc','message'])

aircraftDBfile = "aircraft_db.csv"
aircraftDB = pd.read_csv(aircraftDBfile)
print("ICAO data from Database: ")
searchResult = aircraftDB.loc[aircraftDB['icao'] == ICAOchosen.lower()]
print(searchResult)


def timeFromUnix(unixtime):
    '''Unix to readable date'''
    return datetime.datetime.fromtimestamp(unixtime)


data = data[((data['tc'] >= 9) & (data['tc'] <= 22) & (data['tc'] != 19))]
data = data[data['icao'] == ICAOchosen]


data["unix"] = data["unix"].apply(timeFromUnix)
data["message"] = data["message"].apply(altitude)


# Print all ICAO numbers in the dataset which where below 500ft at one point or another
#data = data[data['message'] < 500]
#print(data.icao.unique())


print(data)

plt.plot(data["unix"],data['message'])
plt.show()
