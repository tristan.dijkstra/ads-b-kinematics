
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import cartopy.crs as ccrs
from cartopy.io import shapereader
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import os

""" ALL OF THIS IS LIKELY TO BE SUPERCEDED BY THE TRAFFIC LIBRARY
hence I've hacked it together towards the end 
just to get something useful in the meantime
"""


def map_subplot(fig,subplotpos=111,extent=[0, 10, 48, 58]):
    """ LIKELY TO BE SUPERCEDED BY THE TRAFFIC MODULE
    """
    ax = fig.add_subplot(subplotpos,projection=ccrs.PlateCarree())
    ax.set_extent(extent)
    gl = ax.gridlines(draw_labels=True)
    gl.xlabels_top = gl.ylabels_right = False
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    
    path = os.path.abspath('.')
    mapfilepath = path+'/MapData/'
    mapfilename = 'netherlands.shp'
    
    shp = shapereader.Reader(mapfilepath+mapfilename)
    for record, geometry in zip(shp.records(), shp.geometries()):
        ax.add_geometries([geometry], ccrs.PlateCarree(), facecolor='w', edgecolor='gray')

    return ax

# Ground Station Location
GSlon,GSlat,GSalt = 4.375374,51.989884,0

def posMap(inputData,marker='',color='b',groundstation=True):
    """ LIKELY TO BE SUPERCEDED BY THE TRAFFIC MODULE

    """ 
    eastwest = '<- West - East ->'
    northsouth = '<- South - North ->'
    if type(inputData) == tuple:
        longitudes = inputData[0]
        latitudes = inputData[1]
    else:
        longitudes = inputData['longitude']
        latitudes = inputData['latitude']

    fig = plt.figure()
    XYplane = plotter.map_subplot(fig)
    XYplane.grid()
    XYplane.set_title('Map')
    XYplane.set_xlabel(eastwest)
    XYplane.set_ylabel(northsouth)
    XYplane.plot(longitudes,latitudes,marker=marker,color=color)
    if groundstation: XYplane.plot(GSlon,GSlat,marker='o',color='r')


def posFigure():
    """ LIKELY TO BE SUPERCEDED BY THE TRAFFIC MODULE
    Arguments:
        None = []
    
    Returns:
        plt.figure object -- [blank figure for the posPlot function to operate on]
    """
    fig = plt.figure()
    XYplane = map_subplot(fig,221)
    XYZspace = fig.add_subplot(222, projection='3d')
    XZplane = fig.add_subplot(223)  
    YZplane = fig.add_subplot(224)
    return (XYplane,XZplane,YZplane,XYZspace)

def posPlot(fig,inputData,marker='',color='b',groundstation=True):
    """ LIKELY TO BE SUPERCEDED BY THE TRAFFIC MODULE
    Creates rudimentary maps from position and altitude data
    Arguments:
        inputData {[pandas Dataframe]} -- [Raw ADS-B dataframe]
    Optional Arguments:
        marker {string} -- places markers on datapoints. Enter 'o' for dots. Set Matplotlib documentation
        color {string}  -- color of the plots. 'r' red, 'b' blue etc. See Matplotlib
        groundstation {bool} -- show groundstation position on the map
    
    Returns:
        None -- []
    """
    if type(inputData) == tuple:
        longitudes = inputData[0]
        latitudes = inputData[1]
        altitudes = inputData[2]
    else:
        longitudes = inputData['longitude']
        latitudes = inputData['latitude']
        altitudes = inputData['altitude']

    # Will add \Leftarrow and \Rightarrow once LaTeX works 
    eastwest = '<- West - East ->'
    northsouth = '<- South - North ->'
    altitudelabel = 'Altitude'

    fig[0].grid()
    fig[0].set_title('Looking Down')
    fig[0].set_xlabel(eastwest)
    fig[0].set_ylabel(northsouth)
    fig[0].plot(longitudes,latitudes,marker=marker)
    if groundstation: fig[0].plot(GSlon,GSlat,marker='o',color='r')


    fig[1].grid()
    fig[1].set_title('Looking North')
    fig[1].set_xlabel(eastwest)
    fig[1].set_ylabel(altitudelabel)
    fig[1].plot(longitudes,altitudes,marker=marker)
    if groundstation: fig[1].plot(GSlon,GSalt,marker='o',color='r')

    fig[2].grid()
    fig[2].set_title('Looking West')
    fig[2].set_xlabel(northsouth)
    fig[2].set_ylabel(altitudelabel)
    fig[2].plot(latitudes,altitudes,marker=marker)
    if groundstation: fig[2].plot(GSlat,GSalt,marker='o',color='r')
    
    fig[3].set_title('Position')
    fig[3].set_xlabel(eastwest)
    fig[3].set_ylabel(northsouth)
    fig[3].set_zlabel(altitudelabel)
    fig[3].plot3D(longitudes,latitudes,altitudes,marker=marker)
    if groundstation: fig[3].scatter3D(GSlon,GSlat,GSalt,color='r')

