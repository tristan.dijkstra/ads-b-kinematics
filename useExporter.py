import exporter
import os
# import plotter

dataPath = "./ads-b-data/"
exporter.exportMultiple(
    "b738", 
    dataPath, 
    printOut=False,
    plotFlights=False
    )



# The following lines are for using exportSingle
# select different files by changing the index on dataFolder. Remember to change the output file accordingly.
# if you only have one file in your folder, just keep the index [0]
# will turn this into a more intuitive function later
# dataFolder = os.listdir(dataPath)
# dataFile = dataPath+dataFolder[0]
# exporter.exportSingle("dh8d", dataFile, True)
